$(function() {
  $('body').css('min-height', $(window).height() );
  $('[data-toggle="tooltip"]').tooltip();

  $('a.delete-confirm').click(function(e) {
    e.preventDefault();
    var $this = $(this);
    var item = $this.data('item') ? $this.data('item') : 'item';
    console.log($this.data('item'))
    var message = "Are you sure you want to delete this " + item + "?";

    bootbox.confirm({
        message: message,
        size: 'medium',
        callback: function(result) {
          if (result)
            window.location.href = $this.attr('href');
        }
    });
  });
});


