<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function(Blueprint $table) {
			$table->dropColumn('username');
			$table->integer('status')->unsigned()->default(1)->after('password');

			// Reverse order
			$table->string('contact_num')->nullable()->after('email');
			$table->string('address_2')->nullable()->after('email');
			$table->string('address_1')->nullable()->after('email');
			$table->string('photo_path')->nullable()->after('email');
			$table->string('last_name', 100)->nullable()->after('email');
			$table->string('middle_initial', 10)->nullable()->after('email');
			$table->string('first_name', 100)->nullable()->after('email');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function(Blueprint $table) {
			$table->string('username')->unique()->after('id');
			$table->dropColumn('first_name');
			$table->dropColumn('middle_initial');
			$table->dropColumn('last_name');
			$table->dropColumn('photo_path');
			$table->dropColumn('address_1');
			$table->dropColumn('address_2');
			$table->dropColumn('status');
		});
	}

}
