<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword, EntrustUserTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'email',
		'password',
		'first_name',
		'last_name',
		'middle_initial',
		'photo_path',
		'address_1',
		'address_2',
		'contact_num',
		'status'
	];

	protected $appends = ['role'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	public function getRoleAttribute()
	{
		if (!$this->roles->isEmpty())
			return $this->roles()->first()->id;
	}

	public function getMiddleInitialAttribute($value)
	{
		// Append period if there isn't
		if (!empty($value) && !preg_match('/(\w+)\.$/', $value))
			return trim($value).'.';

		return $value;
	}

	public function getFullNameAttribute()
	{
		$fullName = $this->first_name . ' ';

		if (isset($this->middle_initial))
			$fullName .= $this->middle_initial . ' ';

		$fullName .= $this->last_name;

		return trim($fullName);
	}

	public function getStatusValueAttribute()
	{
		if ($this->status == 0)
			return 'Inactive';

		return 'Active';
	}

}
