<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Lookups extends Model {

	public $timestamps = false;

	public static function scopeKey($query, $key)
	{
		return $query->whereKey($key);
	}

}
