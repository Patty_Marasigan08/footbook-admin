<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Traits\LookupsValue as LookupsValue;

class ItemProperty extends Model {

	use LookupsValue;

	protected $fillable = ['key', 'value'];

	protected $appends = ['key_name'];

	public function item()
	{
		return $this->belongsTo('App\Item');
	}

	public function getKeyNameAttribute()
	{
		if (isset($this->attributes['key']))
			return $this->getLookupValue($this->attributes['key']);
	}



}
