<?php

Breadcrumbs::register('admin.dashboard', function($breadcrumbs) {
    $breadcrumbs->push('Dashboard', route('admin.dashboard'));
});

Breadcrumbs::register('admin.users.index', function($breadcrumbs) {
	$breadcrumbs->parent('admin.dashboard');
	$breadcrumbs->push('User Management', route('admin.users.index'));
});

Breadcrumbs::register('admin.users.create', function($breadcrumbs) {
	$breadcrumbs->parent('admin.users.index');
	$breadcrumbs->push('Add User', route('admin.users.create'));
});

Breadcrumbs::register('admin.users.show', function($breadcrumbs) {
	$breadcrumbs->parent('admin.users.index');
	$breadcrumbs->push('View User', route('admin.users.show'));
});

Breadcrumbs::register('admin.users.edit', function($breadcrumbs) {
	$breadcrumbs->parent('admin.users.index');
	$breadcrumbs->push('Edit User', route('admin.users.edit'));
});

Breadcrumbs::register('admin.themes.index', function($breadcrumbs) {
	$breadcrumbs->parent('admin.dashboard');
	$breadcrumbs->push('Theme Management', route('admin.themes.index'));
});

Breadcrumbs::register('admin.themes.search', function($breadcrumbs) {
	$breadcrumbs->parent('admin.themes.index');
	$breadcrumbs->push('Search Results', route('admin.themes.search'));
});

Breadcrumbs::register('admin.themes.create', function($breadcrumbs) {
	$breadcrumbs->parent('admin.themes.index');
	$breadcrumbs->push('Add Theme', route('admin.themes.create'));
});

Breadcrumbs::register('admin.themes.show', function($breadcrumbs) {
	$breadcrumbs->parent('admin.themes.index');
	$breadcrumbs->push('View Theme', route('admin.themes.show'));
});

Breadcrumbs::register('admin.themes.edit', function($breadcrumbs) {
	$breadcrumbs->parent('admin.themes.index');
	$breadcrumbs->push('Edit Theme', route('admin.themes.edit'));
});

Breadcrumbs::register('admin.items.index', function($breadcrumbs) {
	$breadcrumbs->parent('admin.dashboard');
	$breadcrumbs->push('Item Management', route('admin.items.index'));
});

Breadcrumbs::register('admin.items.search', function($breadcrumbs) {
	$breadcrumbs->parent('admin.items.index');
	$breadcrumbs->push('Search Results', route('admin.items.search'));
});

Breadcrumbs::register('admin.items.create', function($breadcrumbs) {
	$breadcrumbs->parent('admin.items.index');
	$breadcrumbs->push('Add Item', route('admin.items.create'));
});

Breadcrumbs::register('admin.items.show', function($breadcrumbs) {
	$breadcrumbs->parent('admin.items.index');
	$breadcrumbs->push('View Item', route('admin.items.show'));
});

Breadcrumbs::register('admin.items.edit', function($breadcrumbs) {
	$breadcrumbs->parent('admin.items.index');
	$breadcrumbs->push('Edit Item', route('admin.items.edit'));
});

Breadcrumbs::register('admin.products.index', function($breadcrumbs) {
	$breadcrumbs->parent('admin.dashboard');
	$breadcrumbs->push('Product Management', route('admin.products.index'));
});

Breadcrumbs::register('admin.products.create', function($breadcrumbs) {
	$breadcrumbs->parent('admin.products.index');
	$breadcrumbs->push('Add Product', route('admin.products.create'));
});

Breadcrumbs::register('admin.products.show', function($breadcrumbs) {
	$breadcrumbs->parent('admin.products.index');
	$breadcrumbs->push('View Product', route('admin.products.show'));
});

Breadcrumbs::register('admin.products.edit', function($breadcrumbs) {
	$breadcrumbs->parent('admin.products.index');
	$breadcrumbs->push('Edit Product', route('admin.products.edit'));
});

Breadcrumbs::register('admin.roles.index', function($breadcrumbs) {
	$breadcrumbs->parent('admin.users.index');
	$breadcrumbs->push('Role Management', route('admin.roles.index'));
});

Breadcrumbs::register('admin.roles.create', function($breadcrumbs) {
	$breadcrumbs->parent('admin.roles.index');
	$breadcrumbs->push('Add Role', route('admin.roles.create'));
});

Breadcrumbs::register('admin.roles.show', function($breadcrumbs) {
	$breadcrumbs->parent('admin.roles.index');
	$breadcrumbs->push('View Role', route('admin.roles.show'));
});

Breadcrumbs::register('admin.roles.edit', function($breadcrumbs) {
	$breadcrumbs->parent('admin.roles.index');
	$breadcrumbs->push('Edit Role', route('admin.roles.edit'));
});

Breadcrumbs::register('admin.permissions.edit', function($breadcrumbs) {
	$breadcrumbs->parent('admin.roles.index');
	$breadcrumbs->push('Edit Permissions', route('admin.permissions.edit'));
});

