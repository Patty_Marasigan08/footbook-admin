<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {

	protected $fillable = ['product_path', 'user_id', 'featured'];

	public function items()
	{
		return $this->belongsToMany('App\Item');
	}

	public function properties()
	{
		return $this->hasMany('App\ProductProperty');
	}
}
