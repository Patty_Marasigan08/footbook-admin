var loading_image 	= '<div class="product_slider_loading_wrapper"><img src="skin/frontend/greenwichdelivery/default/images/default_ajax_loader.gif"></a></div>';
var loading_image_2 = '<img src="skin/frontend/greenwichdelivery/default/images/default_ajax_loader_2.gif"></a>';
var loading_image_3 = '<img src="skin/frontend/greenwichdelivery/default/images/default_ajax_loader_3.gif"></a>';
var loading_image_4 = '<img src="skin/frontend/greenwichdelivery/default/images/shopping_cart_loader.gif"></a>';

var TOTAL_ITEM;
var DISPLAYED_ITEM;
var CURRENT_PAGE;
var MAX_PAGE;

var selected_address = 1; // address 1 or address 2 or same as billing address

var ENABLE_HEADER_VALIDATION = true; // for debugging purposes only

function check_device() {
	var user_agent = jQuery.browser.device = (/android|webos|iphone|ipod|blackberry/i.test(navigator.userAgent.toLowerCase()));
	if(user_agent) {
		window.location = "http://razerbite.com/staging-greenwichdelivery/greenwichdelivery-system/mobile/";
	}
}


function searchProducts() {
	var q = jQuery('#searchtextfield').val();

	ready_to_order();
	reset_menu_active();
	jQuery('#product_list_wrapper').html("");
	jQuery('#product_loader').show();
	jQuery('#product_loader').html(loading_image_3);
	jQuery.post('gwd/index/_searchProduct',{q:q},function(o) {
		jQuery('#product_loader').hide();
		jQuery('#product_list_wrapper').html(o);
	});
	
}

function searchProductsByUrlKey() {
	$.blockUI();
	var q = $('#redirect_params').val();
	jQuery('#product_list_wrapper').html("");
	jQuery('#product_loader').show();
	jQuery('#product_loader').html(loading_image_3);
	jQuery.post('gwd/index/_searchProductByUrlKey',{q:q},function(o) {
		$.unblockUI();
		ready_to_order();
		reset_menu_active();
		jQuery('#product_loader').hide();
		jQuery('#product_list_wrapper').html(o);
	});
	
}

function reset_menu_active() {
	/*jQuery('.menu-content').css("opacity",1);
	jQuery('.pizza').removeClass("pizza-active");
	jQuery('.pasta').removeClass("pasta-active");
	jQuery('.meals').removeClass("meals-active");
	jQuery('.sides').removeClass("sides-active");
	jQuery('.drinks').removeClass("drinks-active");
	jQuery('.all').removeClass("all-active");*/

	$('ul.nav-menu li').removeClass('active');
}

/*
Old menu using images
function load_product(category_id) {
	reset_menu_active();
	jQuery('#searchtextfield').val("");
	switch(category_id) {
		case 5 		: 
			jQuery('.pizzaimg').css('opacity',0);
			jQuery('.pizza').addClass("pizza-active");
			break;
		case 6 		:
			jQuery('.pastaimg').css('opacity',0);
			jQuery('.pasta').addClass("pasta-active");
			break;
		case 12 	:
			jQuery('.mealsimg').css('opacity',0);
			jQuery('.meals').addClass("meals-active");
			break;
		case 19 	:
			jQuery('.sidesimg').css('opacity',0);
			jQuery('.sides').addClass("sides-active");
			break;
		case 17 	:
			jQuery('.drinksimg').css('opacity',0);
			jQuery('.drinks').addClass("drinks-active");
			break;
		case "all" 	:
			jQuery('.allimg').css('opacity',0);
			jQuery('.all').addClass("all-active");
			break;
		default:
			jQuery('.pizzaimg').css('opacity',0);
			jQuery('.pizza').addClass("pizza-active");
	}

	jQuery('#product_list_wrapper').html("");
	jQuery('#product_loader').show();
	jQuery('#product_loader').html(loading_image_3);
	
	if(category_id == "all") {
		jQuery.post('gwd/index/load_all_product',{category_id:category_id},function(o) {
			jQuery('#product_loader').hide();
			jQuery('#product_list_wrapper').html(o);
		});	

	} else {
		jQuery.post('gwd/index/_loadProductList',{category_id:category_id},function(o) {
			jQuery('#product_loader').hide();
			jQuery('#product_list_wrapper').html(o);
		});	
	}
}
*/

function load_product(category_id) {
	reset_menu_active();
	jQuery('#searchtextfield').val("");

	jQuery('.li-nav').removeClass('active');
	jQuery('#li_'+category_id).addClass('active');

	jQuery('#product_list_wrapper').html("");
	jQuery('#product_loader').show();
	jQuery('#product_loader').html(loading_image_3);
	
	if(category_id == "all") {
		jQuery.post('gwd/index/load_all_product',{category_id:category_id},function(o) {
			jQuery('#product_loader').hide();
			jQuery('#product_list_wrapper').html(o);
		});	

	} else {
		jQuery.post('gwd/index/_loadProductList',{category_id:category_id},function(o) {
			jQuery('#product_loader').hide();
			jQuery('#product_list_wrapper').html(o);
		});	
	}
}

function show_product_options(product_id) {
	//jQuery('#test_wrapper').html(loading_image_3);
	jQuery.post('gwd/index/showProductOptions',{product_id:product_id},function(o) {
		jQuery('#test_wrapper').html(o);
	});
}

function showItemsCart() {
	jQuery('#cart_items_wrapper').html("");
	jQuery('#cart_loader').show();
	jQuery.post('onepagecheckout/cart/_showCartItems',{},function(o) {
		jQuery('#cart_loader').hide();
		jQuery('#cart_items_wrapper').html(o);
	});
}


function removeCartItem(item_id) {
	jQuery('#cart_item_'+item_id).remove();
	jQuery('#receipt_item_'+item_id).remove();

	$.blockUI();
	jQuery.post('onepagecheckout/cart/_removeCartItem',{item_id:item_id},function(o) {
		//showItemsCart();
		$.unblockUI();
		if(!o.has_cart_items) {
			jQuery('#cart_items_wrapper').html(o.message)
		}
		jQuery('#grand_total_wrapper').html(o.grand_total);
	},'json');
}

function showSidebarReceiptCartItems() {
	jQuery('#receipt_item_list_wrapper').html(loading_image_4);
	jQuery.post('onepagecheckout/cart/_showSidebarReceiptCartItems',{},function(o) {
		jQuery('#receipt_item_list_wrapper').html(o);
	});
}

function showPopupReceiptCartItems() {
	jQuery('#receipt_item_list_modal_wrapper').html(loading_image_4);
	jQuery.post('onepagecheckout/cart/_showPopupReceiptCartItems',{},function(o) {
		jQuery('#receipt_item_list_modal_wrapper').html(o);
	});
}


function ready_to_order() {
	clearInlineValidationInstance();

	jQuery('.step_hideshow').hide();
	jQuery('#step1').slideToggle();
	showItemsCart();
}

function show_step(next_step) {
	clearInlineValidationInstance();

	if(ENABLE_HEADER_VALIDATION == false) {
		jQuery('.step_hideshow').hide();
		jQuery('#step'+next_step).slideToggle();
	} else {
		$.blockUI();
		jQuery.post('gwd/index/validate_next_step',{next_step:next_step},function(o) {
			$.unblockUI();
			if(o.step_cleared == true) {
				jQuery('.step_hideshow').hide();
				jQuery('#step'+next_step).slideToggle();
				jQuery('html,body').animate({scrollTop: jQuery(".step"+next_step).offset().top},100);

				show_my_account_info();

				if(next_step == 2) {
					jQuery('#email_address').focus();
				}
			} else {
				alert('Please complete the steps');
			}
		},'json');
	}

}

function clear_steps() {
	jQuery('.step_hideshow').hide();
	jQuery('#step0').show();
}

function customer_logout() {
	window.location.href = 'gwd/index/customer_logout';
}

function show_logging_in() {
	jQuery('#signup_form_wrapper').hide();
	jQuery('#logging_in_wrapper').show();
}

function logged_in_user(data) {
    var logged_user = data.firstname;
    var name    	= data.name;
    var a_name    	= data.a_name;
    var company_name= data.company_name;
    var street1  	= (data.street1 ? data.street1 : "");
    var street2  	= (data.street2 ? "<br/>" + data.street2 : "");
    var street3  	= (data.street3 ? "<br/>" + data.street3 : "");
    var city    	= data.city;
    var phone   	= data.telephone;
    var mobile 		= data.mobile;
    var postcode 	= data.postcode;

    jQuery('#delivery_receipt_info').removeClass("hidden");

    jQuery('#deliver_to').html(a_name);
    jQuery('#deliver_to_company_name').html( (!company_name ? "" : company_name + "<br/>") );
    jQuery('#deliver_to_address').html(street1  + street2 + street3);
    jQuery('#deliver_to_city').html("<br/>" + city);
    jQuery('#deliver_phone').html(phone);
    jQuery('#deliver_mobile').html(mobile);
    jQuery('#logged_user_wrapper').html('Hello, '+logged_user+ ' | <a class="quick_my_account_link link" href="javascript:void(0);" onclick="javascript:show_my_account_quick();">MY ACCOUNT</a> | <a class="quick_my_account_link link" href="javascript:void(0);" onclick="javascript:customer_logout();">SIGN OUT</a> ');
    jQuery('#is_logged_content_wrapper').show(); // show logged_in status form

    show_my_account_info();
}

function show_addresses(address){
	jQuery('.address_wrapper').hide();
	jQuery('#address_'+address+'_wrapper').show();
	selected_address = address;

	jQuery('.click2').removeClass("location-active");
	jQuery('#address_'+ address +'_link').addClass("location-active");
}

function show_whats_hot()  {
	clearInlineValidationInstance();

	jQuery('.step_hideshow').hide();
	jQuery('#step0').slideToggle();
}

function cancel_transaction() {
	show_whats_hot();
}

function show_placeorder() {
	var payment_change_for 			= jQuery('#payment_change_for').val();
	var payment_other_instructions 	= jQuery('#payment_other_instructions').val();

	var customer_address_id 		= sessionStorage.getItem("customer_address_id");
	customer_address_id = (customer_address_id == 'NaN' ? '' : customer_address_id);

	sessionStorage.setItem("payment_change_for", payment_change_for);
	sessionStorage.setItem("payment_other_instructions", payment_other_instructions);

	jQuery('#instruction_content').html(payment_other_instructions);
	jQuery('#light_resibo_wrapper').html(loading_image_3);
	jQuery.post('gwd/index/show_placeorder',{customer_address_id:customer_address_id},function(o) {
		jQuery('#light_resibo_wrapper').html(o);
		
	})
	
}

function submit_place_order() {
	jQuery('#final_place_order_form').submit();
}

function get_last_purchase_order1(order_number) {
	//window.location = "order/reorder_order?order_number="+order_number;
	$.blockUI();
	jQuery.post("gwd/index/reorder_order?order_number="+order_number,{},function(o) {
		$.unblockUI();
		if(o.is_successful) {
			show_item_cart();
            show_resibo_rightsidebar();

            if(o.total_items <= 0) {
				alert('Sorry! Server is busy processing the order. Please click the reoder button again.');
			}
		} else {
			alert(o.message);
		}
	},'json');

	/*
	$.blockUI();
	jQuery.post('order/get_last_purchase_order',{},function(o) {
		$.unblockUI();
		if(o.is_successful) {
			show_item_cart();
            show_resibo_rightsidebar();
		} else {
			alert(o.message);
		}
	},'json');
	*/
}

function update_address_1() {
	var address 	= jQuery('#street_address_1_1').val();
	var city 		= jQuery('#city_section_1').val();
	var postcode 	= jQuery('#postcode').val();
	var telephone	= jQuery('#telephone').val();

	jQuery.post('gwd/index/update_address_1',{address:address,city:city,postcode:postcode,telephone:telephone},function() {});
}

function update_address_2() {
	var address 	= jQuery('#street_address_2_1').val();
	var city 		= jQuery('#city_section_2').val();
	var postcode 	= jQuery('#postcode').val();
	var telephone	= jQuery('#telephone').val();

	jQuery.post('gwd/index/update_address_2',{address:address,city:city,postcode:postcode,telephone:telephone},function() {});
}

function show_receipt_items_popup() {
	jQuery('#resibo_item_list_popup_wrapper').html("");
	jQuery('#resibo_loader_popup').show();
	jQuery('#resibo_loader_popup').html(loading_image_4);
	jQuery.post('gwd/index/show_placeorder_receipt',{},function(o) {
		jQuery('#resibo_loader_popup').hide();
		jQuery('#resibo_item_list_popup_wrapper').html(o);
	});
}

function show_my_account_info(showOverride) {

	jQuery('#my_account_wrapper').html("");
	jQuery('#my_account_info_loading_wrapper').show();
	jQuery('#my_account_info_loading_wrapper').html(loading_image_3);
	jQuery.post('gwd/index/my_account_info',{},function(o) {
        jQuery('#my_account_info_loading_wrapper').hide();
        jQuery('#my_account_wrapper').html(o);
        jQuery('#my_account_wrapper').show();
        // address_book_list();
        my_orders();
    });

	/*
	var visible = jQuery('#my_account_wrapper').is(":visible");

    if(!visible) {
	    jQuery('#my_account_info_loading_wrapper').show();
	    jQuery('#my_account_info_loading_wrapper').html(loading_image_3);
	}

	if(showOverride == 1) { jQuery('#my_account_wrapper').hide(); }
    jQuery('#my_account_wrapper').toggle({
        duration : 10,
        complete : function() {
            var visible = jQuery('#my_account_wrapper').is(":visible");

            if(visible) {
                jQuery.post('gwd/index/my_account_info',{},function(o) {
                    jQuery('#my_account_info_loading_wrapper').hide();
                    jQuery('#my_account_wrapper').html(o);
                    // address_book_list();
                    my_orders();
                });

            }
        }
    });*/
}

function show_my_account_details(sidebar) {

	clearInlineValidationInstance();

	jQuery(".my_account_content").hide();
    jQuery("#edit_billing_address_form_wrapper").hide();
    jQuery(".my_account_content").removeClass("user-active");

    jQuery("#"+sidebar+"_wrapper").show();
    jQuery(".my_account_sidebar_link").removeClass("user-active");
    jQuery("#"+sidebar).addClass("user-active");

    switch(sidebar) {
    	case "account_dashboard_sidebar" 	: account_information_dashboard();  break;
    	case "account_information_sidebar" 	: account_information_form(); break;
    	case "my_orders_sidebar" 			: address_book_list(); break;
    	default : my_orders();
    }
}

function my_orders() {
	jQuery('#order_history_details_wrapper').html("");
	jQuery('#my_orders_wrapper').html(loading_image_3);
	jQuery.post('onepagecheckout/order/_orderHistory',{},function(o) {
	    jQuery('#my_orders_wrapper').html(o);
	});
}

function account_information_dashboard() {
	$.blockUI();
	//jQuery('#contact_information_dashboard_wrapper').html(loading_image_3);
	jQuery.post('gwd/index/account_information_dashboard',{},function(o) {

		$.unblockUI();
		jQuery('#contact_information_dashboard_wrapper').show();
	    jQuery('#contact_information_dashboard_wrapper').html(o);
	    my_orders();
	});
}

function address_book_list() {

    jQuery('#edit_billing_address_form_wrapper').hide();

    jQuery('#address_book_main_wrapper').show();
    jQuery('#contact_information_dashboard_wrapper').show();

    jQuery('#address_book_list_wrapper').hide();
    jQuery('#address_book_list_loader_wrapper').show();
    jQuery('#address_book_list_loader_wrapper').html(loading_image_3);
    
    jQuery.post('gwd/index/address_book_list',{},function(o) {
        jQuery('#address_book_list_loader_wrapper').hide();
        jQuery('#address_book_list_wrapper').show();
        jQuery('#address_book_list_wrapper').html(o);
    });
    
}

function add_address() {

	clearInlineValidationInstance();

	jQuery('#address_book_list_wrapper').hide();
	jQuery('#address_book_list_loader_wrapper').show();
	jQuery('#address_book_list_loader_wrapper').html(loading_image_3);

	jQuery.post('gwd/index/add_address',{},function(o) {
		jQuery('#address_book_list_loader_wrapper').hide();
		jQuery('#address_book_list_wrapper').show();
	    jQuery('#address_book_list_wrapper').html(o);
	});

}


function edit_address_book(id) {
	var id = parseInt(id);

	clearInlineValidationInstance();

	jQuery('#address_book_list_wrapper').hide();
	jQuery('#address_book_list_loader_wrapper').show();
	jQuery('#address_book_list_loader_wrapper').html(loading_image_3);

	jQuery.post('gwd/index/edit_address_book',{id:id},function(o) {
		jQuery('#address_book_list_loader_wrapper').hide();
		jQuery('#address_book_list_wrapper').show();
	    jQuery('#address_book_list_wrapper').html(o);
	});
}

function clearInlineValidationInstance() {

	//removes all inline validation prompt instances
	$('#signup_form').validationEngine('hide');
	$('.validate-form').validationEngine('hide');
	$('#account_information_form').validationEngine('hide');
	$('#edit_billing_address').validationEngine('hide');
	$('#add_address_s2').validationEngine('hide');
	$('#edit_address_book_s2').validationEngine('hide');
	$('#add_address_signupstep3').validationEngine('hide');

	//clear address_book_list_wrapper to prevent overlapping of instance
	$('#edit_billing_address_form_wrapper').html("");
	$('#address_book_list_wrapper').html("");
}

function delete_address(id) {
	var id = parseInt(id);
	jQuery.post('gwd/index/delete_address',{id:id},function(o) {
		if(o.is_successful) {
			address_book_list();
		}
	},'json');
}

function account_information_form() {
	
	jQuery('#account_information_form_wrapper').html(loading_image_3);
	jQuery.post('gwd/index/account_information_form',{},function(o) {
	    jQuery('#account_information_form_wrapper').html(o);
	});
}

function whats_your_address() {
	clearInlineValidationInstance();
	show_all_customer_address_dropdown();
}

function show_all_customer_address_dropdown() {

	var next_step = 3;
	if(ENABLE_HEADER_VALIDATION == false) {
		jQuery.post('gwd/index/validate_next_step',{next_step:next_step},function(o) {
			if(o.step_cleared == true) {
				jQuery.post('gwd/index/show_all_customer_address',{},function(o) {
					load_all_customer_address();
				});
			} else {
				alert(o.message);
			}
		},'json');
	} else {
		$.blockUI();
		jQuery.post('gwd/index/validate_next_step',{next_step:next_step},function(o) {
			if(o.step_cleared == true) {
				jQuery.post('gwd/index/show_all_customer_address',{},function(o) {
					$.unblockUI();
					load_all_customer_address();
				});
			} else {
				$.unblockUI();
				alert(o.message);
			}
		},'json');
	}
}

function load_all_customer_address() {
	jQuery.post('gwd/index/show_all_customer_address',{},function(o) {
		if(ENABLE_HEADER_VALIDATION == true) {
		$.unblockUI();	
		}
		jQuery('.step_hideshow').hide();
		jQuery('#step3').slideToggle();
		jQuery('html,body').animate({scrollTop: jQuery(".step3").offset().top},100);
		jQuery('#address_list_wrapper').html(o);
	});
}

function load_receipt_sidebar_info(customer_address_id) {
	var customer_address_id = parseInt(customer_address_id);
	jQuery.post('gwd/index/load_receipt_sidebar_info',{customer_address_id:customer_address_id},function(o) {
	    jQuery('#delivery_receipt_info_wrapper').html(o);
	});
}


function saveShipping() {
	//Save Shipping address 
	var data = jQuery('#aitoc_rewrite_saveshipping_form').serialize();
	jQuery.post('checkout/onepage/saveShipping', data,function() {});


	//get stored happyplus number of customer
	var happyplus 		= $('.happyplus_field').val();
	if(happyplus) {
		var addr_id   		= $('#shipping_address_id').val();
		var attribute_code 	= "happyplus_card_number";
		jQuery.post('gwd/index/storeHappy', {happyplus:happyplus, addr_id:addr_id, attribute_code:attribute_code} ,function() {});
	}
	
}

function proceed_to_checkout(message) {
	clearInlineValidationInstance();

	var next_step = 4;
	if(!isNaN(jQuery('#customer_address').val())) {
		if (isCustomFieldsValidated() ) { 
			$.blockUI();
			saveShipping();
			var customer_address_id = jQuery('#customer_address').val();

			if(customer_address_id != "")
			jQuery.post('gwd/index/validate_next_step',{next_step:next_step, customer_address_id:customer_address_id},function(o) {
				if(o.step_cleared == true) {
					$.unblockUI();

					sessionStorage.setItem("customer_address_id", customer_address_id);

					if(customer_address_id != "") {
						load_receipt_sidebar_info(customer_address_id);
					}

					jQuery('.step_hideshow').hide();
					jQuery('#step4').slideToggle();

				} else {
					$.unblockUI();
					alert(o.message);
				}
			},'json');
		}
	} else {
		alert("Can't proceed to this step - Please select your shipping address in dropdown on step 4 first.");
	}
	
	/*
	var street_address 	= jQuery('#street_address_'+selected_address+'_1').val();
	var city 			= jQuery('#city_section_'+selected_address).val();

	sessionStorage.setItem("street_address", street_address);
	sessionStorage.setItem("city", city);

	if(street_address != "" && city  != "") {

		//UPDATES ADDRESS
		if(selected_address == 1) {
			update_address_1();
		} else {
			update_address_2();
		}

		jQuery('#deliver_address_sidebar').html(street_address);
		jQuery('#deliver_city_sidebar').html(city);

		jQuery('.step_hideshow').hide();
		jQuery('#step4').slideToggle();
	} else {
		alert("Please input your Billing / Delivery Address");
	}
	*/
}

function order_history_details(order_number) {
	jQuery('.recent_order').removeClass('order-viewed-active');
	jQuery('#recent_order_'+order_number).addClass('order-viewed-active');

	jQuery('#order_history_details_wrapper').html(loading_image_3);
	jQuery.post('onepagecheckout/order/_orderHistoryDetails',{order_number:order_number},function(o) {
	    jQuery('#order_history_details_wrapper').html(o);
	});
}

function add_new_address_step3() {
	jQuery('#address_form_wrapper').hide();
	jQuery('#customer_address_wrapper').hide();
	jQuery('#add_address_form_wrapper').show();
}


function cancel_add_address_step3() {
	clearInlineValidationInstance();

	jQuery('#address_form_wrapper').show();
	jQuery('#customer_address_wrapper').show();
	jQuery('#add_address_form_wrapper').hide();
}

/*
jQuery(".edit_billing_add_link").live('click', function() {
    edit_billing_address();
});
*/

function edit_billing_address(id) {
    jQuery('#contact_information_dashboard_wrapper').hide();
    jQuery('#edit_billing_address_form_wrapper').show();

    jQuery('#edit_billing_address_form_wrapper').html(loading_image_3);
    jQuery.post('gwd/index/edit_billing_address',{id:id},function(o) {
       jQuery('#edit_billing_address_form_wrapper').html(o);
    });
}

function show_my_account_quick() {
	clearInlineValidationInstance();

	var next_step = 2;
	if(ENABLE_HEADER_VALIDATION == false) {
		jQuery('.step_hideshow').hide();
		jQuery('#step'+next_step).slideToggle();
	} else {
		$.blockUI();
		jQuery.post('gwd/index/validate_next_step',{next_step:next_step},function(o) {
			$.unblockUI();
			if(o.step_cleared == true) {
				jQuery('.step_hideshow').hide();
				jQuery('#step'+next_step).slideToggle();
				jQuery('html,body').animate({scrollTop: jQuery(".step"+next_step).offset().top},100);

				show_my_account_info(1);

			} else {
				alert('Please complete the steps');
			}
		},'json');
	}
	
}


function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}