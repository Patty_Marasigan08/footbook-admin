function overrideAction(e) {
	var action = $('#redirect_override').val();
	
	switch(action) {
		case 'step2' : show_step(2); break;
		case 'my_account' : show_my_account_quick(); break;
		case 'promos' :
			ready_to_order();
			load_product('33');
		break;
	 	case 'resetpassword' :
	 		$.blockUI();
	 		jQuery.post('gwd/customer/_resetPassword',{},function(o) {
	 			$.unblockUI();
	 			if(o.is_successful) {

	 				var next_step = 2;
	 				jQuery('.step_hideshow').hide();
					jQuery('#step'+next_step).slideToggle();
					jQuery('html,body').animate({scrollTop: jQuery(".step"+next_step).offset().top},100);

	 				showCreateNewPasswordForm();
	 				
	 			} else {
	 				alert(o.message);
	 			}
			},'json');
		break;
		case 'viewproductdirect' : searchProductsByUrlKey(); break;
			
		default : break;
	}

	$('#redirect_override').val("");
}

function showCreateNewPasswordForm() {
	$("#signup_form").validationEngine("hide");
    jQuery('#signup_form_wrapper').hide();
    jQuery('#user_login_form_wrapper').hide();
    jQuery('#forgotpassword_form_wrapper').hide();
    
    jQuery.post('gwd/customer/_resetPasswordForm',{},function(o) {
       jQuery('#createnewpass_form_wrapper').html(o);
    });
	
}