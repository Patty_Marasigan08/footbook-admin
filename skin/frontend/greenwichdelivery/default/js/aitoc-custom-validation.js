var totalErrorCount = 0;
var errorLog;

function isCustomFieldsValidated() {
	totalErrorCount = 0;
	errorLog = new Array();

	$('.gwdaitocfield').each(function() {
		$this = $(this);
		var tagName = this.tagName.toLowerCase();
		
		// get tag name : input, select, textarea
		switch(tagName) {
			case 'input' 	: 
				var attr = $this.attr('type');
				validateField($this);

			break;

			case 'textarea' :
			case 'select'   : validateField($this); break;
		}
	});

	if(totalErrorCount != 0) {
		
		showErrorMessage();
		return false;
	} else {
		$('#aitoc_custom_error_message_wrapper').hide();
		return true; 
	}
}

function showErrorMessage() {
	var finalErrorLog = [];
	$.each(errorLog, function(i, el){
	    if($.inArray(el, finalErrorLog) === -1) finalErrorLog.push(el);
	});
	
	var html = "<ul>";
	$.each(finalErrorLog,function(i, data) {
		html += "<li>" + data + "</li>";
	});

	$('#aitoc_custom_error_message_wrapper').show();
	$('#aitoc_custom_error_message_wrapper').html(html);
	console.log(finalErrorLog);
}

function validateField(obj) {

	var value 	= obj.val();
	var attr 	= $this.attr('type');

	// default : validate required
	if( obj.hasClass('required-entry') ) {
		triggerValidate(value,'required-entry',obj);
	}

	if( obj.hasClass('validate-one-required-by-name')) {
		
		var labelText 	= $('label[for="' + obj.attr('group_id') + '"]').text();
		var taggedClass = obj.attr('group_id').replace(":","-");
		var className 	= "."+taggedClass;

		if($(className+':checked').size() === 0) {
			message = "<strong>'" + labelText + "'</strong> options is required. Please choose one.";
			errorLog.push(message);
			totalErrorCount++;
		}
	}

	//validate decimal number
	if( obj.hasClass('validate-number') ) {
		triggerValidate(value,'validate-number',obj);
	}
	// validate int number
	if( obj.hasClass('validate-digits') ) {
		triggerValidate(value,'validate-digits',obj);
	}

	// validate int number
	if( obj.hasClass('validate-email') ) {
		triggerValidate(value,'validate-email',obj);
	}

	// validate url number
	if( obj.hasClass('validate-url') ) {
		triggerValidate(value,'validate-url',obj);
	}

	// validate letters
	if( obj.hasClass('validate-alpha') ) {
		triggerValidate(value,'validate-alpha',obj);
	}

	// validate letters and numbers
	if( obj.hasClass('validate-alphanum') ) {
		triggerValidate(value,'validate-alphanum',obj);
	}
}

function triggerValidate(value,validation_type,obj) {
	var message 	= "";
	var labelText 	= $('label[for="' + obj.attr('id') + '"]').text();
	switch(validation_type) {
		case 'required-entry' :
			if(value === "") {
				totalErrorCount++;

				message = "<strong>'" + labelText + "'</strong> is required.";
				errorLog.push(message);

				//obj.addClass('aitoc-error');
			} else { obj.removeClass('aitoc-error'); } 
		break;

		case 'validate-one-required-by-name' :
			if(value === "") {
				totalErrorCount++;

				message = "<strong>'" + labelText + "'</strong> is required. Please choose one.";
				errorLog.push(message);

				//obj.addClass('aitoc-error');
			} else { obj.removeClass('aitoc-error'); }
		break;

		case 'validate-number' :
			var rx = new RegExp(/^\d+(?:\.\d{1,2})?$/);
			if (!rx.test(value)) {
				totalErrorCount++;

				message = "<strong>'" + labelText + "'</strong> only accepts 2 digit decimal number.";
				errorLog.push(message);

				//obj.addClass('aitoc-error');
			} else { obj.removeClass('aitoc-error'); }
		break;

		case 'validate-digits' : 
			var rx = new RegExp(/^[-+]?\d+$/);
			if (!rx.test(value)) {
				totalErrorCount++;

				message = "<strong>'" + labelText + "'</strong> only accepts valid positive number.";
				errorLog.push(message);

				//obj.addClass('aitoc-error');
			} else { obj.removeClass('aitoc-error'); }
		break;

		case 'validate-email' : 
			var rx = new RegExp(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/);
			if (!rx.test(value)) {
				totalErrorCount++;

				message = "<strong>'" + labelText + "'</strong> only accepts valid email address.";
				errorLog.push(message);

				//obj.addClass('aitoc-error');
			} else { obj.removeClass('aitoc-error'); }
		break;

		case 'validate-url' : 
			var rx = new RegExp(/^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i);
			if (!rx.test(value)) {
				totalErrorCount++;

				message = "<strong>'" + labelText + "'</strong> only accepts valid url.";
				errorLog.push(message);

				//obj.addClass('aitoc-error');
			} else { obj.removeClass('aitoc-error'); }
		break;

		case 'validate-alpha' : 
			var rx = new RegExp(/^[a-zA-Z\ \']+$/);
			if (!rx.test(value)) {
				totalErrorCount++;

				message = "<strong>'" + labelText + "'</strong> only accepts letters.";
				errorLog.push(message);

				//obj.addClass('aitoc-error');
			} else { obj.removeClass('aitoc-error'); }
		break;

		case 'validate-alphanum' : 
			var rx = new RegExp(/^[0-9a-zA-Z]+$/);
			if (!rx.test(value)) {
				totalErrorCount++;

				message = "<strong>'" + labelText + "'</strong> only accepts letters or numbers.";
				errorLog.push(message);

				//obj.addClass('aitoc-error');
			} else { obj.removeClass('aitoc-error'); }
		break;

		default : break;
	}
}