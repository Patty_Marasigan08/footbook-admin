@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
            {!! Breadcrumbs::render() !!}
			<div class="panel panel-default">
				<div class="panel-heading">ITEM MANAGEMENT</div>
				<div class="panel-body">

                    <div class="row paging">
                        <div class="col-md-6">
                            @if (isset($query))
                                {!! $items->appends(['q'=>$query])->render() !!}
                            @else
                                {!! $items->render() !!}
                            @endif
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="{{ route('admin.items.create') }}" class="btn btn-primary">Add Item</a>
                        </div>
                    </div>

                    <div id="custom-search-input" class="paging">
                        {!! Form::open([ 'method' => 'GET', 'route' => 'admin.items.search' ]) !!}
                        <div class="input-group col-md-12">
                        {!! Form::text('q', null, ['class'=>'form-control input-xs', 'placeholder'=>'Search items']) !!}
                            <span class="input-group-btn">
                                <button class="btn btn-info btn-xs" type="submit">
                                    <i class="glyphicon glyphicon-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}
                    </div>

                    @if ($items->isEmpty())
                        <p class="alert alert-danger">No items found</p>
                    @else

                    <table class="table table-responsive">
                        <tr>
                            <th>ID</th>
                            <th>Type</th>
                            <th>Texture</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th></th>
                        </tr>

                        @foreach ($items as $item)
                        <tr>
                            <td>{!! $item['item_id'] !!}</td>
                            <td>{!! $item['type_name'] !!}</td>
                            <td><img src="{{ asset($item['texture_path']) }}" width="50" height="50"></td>
                            <td><p>{!! $item['description'] !!}</p></td>
                            <td><p>{!! $item['price'] !!}</p></td>
                            <td class="text-right">
                                <a href="{{ route('admin.items.show', [$item]) }}" class="btn btn-default btn-xs"
                                    data-toggle="tooltip" data-placement="top" title="View">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </a>
                                <a href="{{ route('admin.items.edit', [$item]) }}" class="btn btn-default btn-xs"
                                    data-toggle="tooltip" data-placement="top" title="Edit">
                                    <span class="glyphicon glyphicon-pencil"></span>
                                </a>
                                <a href="{{ route('admin.items.destroy', [$item]) }}" class="btn btn-default btn-xs btn-danger delete-confirm"
                                        data-toggle="tooltip" data-placement="top" title="Delete">
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>
                            </td>
                        </tr>

                        @endforeach
                    </table>
                    @endif

                    <div class="row paging">
                        <div class="col-md-6">
                            @if (isset($query))
                                {!! $items->appends(['q'=>$query])->render() !!}
                            @else
                                {!! $items->render() !!}
                            @endif
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="{{ route('admin.items.create') }}" class="btn btn-primary">Add Item</a>
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
