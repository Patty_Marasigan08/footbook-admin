@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                {!! Breadcrumbs::render() !!}
                <div class="panel panel-default">
                    <div class="panel-heading">VIEW ROLE</div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <table class="table table-striped">
                                    <tr>
                                        <td>Name:</td>
                                        <td>{{ $role->name }}</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-xs-12 text-right">
                                <a href="{{ route('admin.roles.edit', [$role->id]) }}" class="btn btn-primary">Edit</a>
                                <a href="{{ route('admin.roles.index') }}" class="btn btn-primary">Back</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
