@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
            {!! Breadcrumbs::render() !!}
			<div class="panel panel-default">
				<div class="panel-heading">THEME MANAGEMENT</div>

				<div class="panel-body">

                    <div class="row paging">
                        <div class="col-md-6">
                            @if (isset($query))
                                {!! $themes->appends(['q'=>$query])->render() !!}
                            @else
                                {!! $themes->render() !!}
                            @endif
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="{{ route('admin.themes.create') }}" class="btn btn-primary">Add Theme</a>
                        </div>
                    </div>

                    <div id="custom-search-input" class="paging">
                        {!! Form::open([ 'method' => 'GET', 'route' => 'admin.themes.search' ]) !!}
                        <div class="input-group col-md-12">
                        {!! Form::text('q', null, ['class'=>'form-control input-xs', 'placeholder'=>'Search themes']) !!}
                            <span class="input-group-btn">
                                <button class="btn btn-info btn-xs" type="submit">
                                    <i class="glyphicon glyphicon-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}
                    </div>

                    <table class="table table-responsive">
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th></th>
                        </tr>

                        @foreach ($themes as $theme)
                        <tr>
                            <td>{!! $theme['theme_id'] !!}</td>
                            <td>{!! $theme['name'] !!}</td>
                            <td><p>{!! $theme['description'] !!}</p></td>
                            <td class="text-right">
                                <a href="{{ route('admin.themes.show', [$theme]) }}" class="btn btn-default btn-xs"
                                   data-toggle="tooltip" data-placement="top" title="View">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </a>
                                <a href="{{ route('admin.themes.edit', [$theme]) }}" class="btn btn-default btn-xs"
                                   data-toggle="tooltip" data-placement="top" title="Edit">
                                    <span class="glyphicon glyphicon-pencil"></span>
                                </a>
                                <a href="{{ route('admin.themes.destroy', [$theme]) }}" class="btn btn-default btn-xs btn-danger delete-confirm"
                                   data-toggle="tooltip" data-placement="top" title="Delete" data-item="theme">
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>
                            </td>
                        </tr>

                        @endforeach

                    </table>
                    <div class="row paging">
                        <div class="col-md-6">
                            @if (isset($query))
                                {!! $themes->appends(['q'=>$query])->render() !!}
                            @else
                                {!! $themes->render() !!}
                            @endif
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="{{ route('admin.themes.create') }}" class="btn btn-primary">Add Theme</a>
                        </div>
                    </div>				</div>
			</div>
		</div>
	</div>
</div>
@endsection
