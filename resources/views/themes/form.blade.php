<div class="row">
    <fieldset>



    <!-- Text input-->
    <div class="form-group">
      <label class="col-md-4 control-label" for="theme_id">Theme ID</label>
      <div class="col-md-4">
      {!! Form::text('theme_id', null, ['class'=>'form-control input-md', 'required']) !!}
      </div>
    </div>

    <!-- Text input-->
    <div class="form-group">
      <label class="col-md-4 control-label" for="name">Theme Name</label>
      <div class="col-md-4">
      {!! Form::text('name', null, ['class'=>'form-control input-md', 'required']) !!}
      </div>
    </div>

    <!-- Textarea -->
    <div class="form-group">
      <label class="col-md-4 control-label" for="description">Description</label>
      <div class="col-md-4">
      {!! Form::textarea('description', null, ['class'=>'form-control']) !!}
      </div>
    </div>

    <!-- Select Basic -->
    <div class="form-group">
      <label class="col-md-4 control-label" for="type">Age Demographic</label>
      <div class="col-md-4">
        {!! Form::select('age_demographic', [''=>'Please Select']+$ageDemographicsList, null, ['class'=>'form-control']) !!}
      </div>
    </div>

    <!-- Select Basic -->
    <div class="form-group">
      <label class="col-md-4 control-label" for="type">Age Range</label>
      <div class="col-md-4">
        {!! Form::select('age_range', [''=>'Please Select']+$ageRangeList, null, ['class'=>'form-control']) !!}
      </div>
    </div>

    <!-- Multiple Radios -->
    <div class="form-group">
      <label class="col-md-4 control-label" for="properties[material_type]">Gender</label>
      <div class="col-md-4">
      <div class="radio-inline">
        <label>
          {!! Form::radio('gender', 'M') !!}
          Male
        </label>
      </div>
      <div class="radio-inline">
        <label>
          {!! Form::radio('gender', 'F') !!}
          Female
        </label>
      </div>
      </div>
    </div>

    <!-- File Button -->
    <div class="form-group">
      <label class="col-md-4 control-label" for="background">Background</label>
      <div class="col-md-4">
        @if (isset($theme['background_path']))
          <img src="{{ asset($theme['background_path']) }}" style="max-width: 100%; margin-bottom: 1em;" />
        @endif
        <input id="background" name="background" class="input-file" type="file" />
      </div>
    </div>

    </fieldset>

</div>
